const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    sizeOfProduct: {
        type: String,
        require: true
    },
    image: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    city: {
        type: String,
        require: true
    },
    message: {
        type: String,
        require: true
    },
    dateOfCreateProduct: {
        type: Date,
        default: Date.now,
        require: true
    }
});

const Product = mongoose.model('Product', productSchema);
module.exports = Product;