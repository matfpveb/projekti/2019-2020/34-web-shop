const mongoose = require('mongoose');

const serviceSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    sizeOfservice: {
        type: String,
        require: true
    },
    image: {
        type: String,
        require: true
    },
    name: {
        type: String,
        require: true
    },
    city: {
        type: String,
        require: true
    },
    message: {
        type: String,
        require: true
    },
    dateOfCreateProduct: {
        type: Date,
        default: Date.now,
        require: true
    }
});

const Service = mongoose.model('Service', serviceSchema);
module.exports = Service;