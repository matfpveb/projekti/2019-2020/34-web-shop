const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        require: true
    },
    secretToken: {
        type: String,
        require: true
    },
    active: {
        type: Boolean,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    admin: {
        type: Boolean,
        require: true
    }
});

const User = mongoose.model('User', userSchema);
module.exports = User;