const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (_req, _file, cb) {
        cb(null, '../client/public/');
    },
    filename: function (_req, file, cb) {
        cb(null, file.originalname);
    }
});

const fileFilter = (_req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
        cb(null, true);
    } else {
        cb(new Error("Nepodrzan format!"), false);
    }
};

const upload = multer({
    storage, limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

module.exports = upload;