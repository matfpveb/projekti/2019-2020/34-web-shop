const express = require('express');
const router = express.Router();
const productController = require('../../controllers/product');
const userController = require('../../controllers/user');
const serviceController = require('../../controllers/service');
const upload = require('../../middleware/uploadImage');

router.post('/product/save', upload.single('image'), productController.saveProduct);
router.get('/product/getAllProducts', productController.getAllProducts);
router.get('/product/findById/:id', productController.findById);
router.get('/product/findByName/:name', productController.findByName);
router.get('/product/findByCity/:city', productController.findByCity);
router.get('/product/listOfCities', productController.listOfCities);
router.get('/mostCommonCity', productController.mostCommonCity);

router.post('/service/save', upload.single('image'), serviceController.saveService);
router.get('/service/getAllServices', serviceController.getAllServices);
router.get('/service/findById/:id', serviceController.findById);
router.get('/service/findByName/:name', serviceController.findByName);
router.get('/service/findByCity/:city', serviceController.findByCity);
router.get('/service/listOfCities', serviceController.listOfCities);

router.post('/newUser', userController.addNewUser);
router.post('/sendVerifyToken', userController.sendVerifyToken);
router.post('/verify', userController.verify);
router.post('/login', userController.findUser);
router.post('/contact', userController.sendMsg);
router.post('/findUserByEmail', userController.findUserByEmail);
router.post('/checkToken', userController.checkToken);
router.get('/users', userController.listUsers);

module.exports = router;