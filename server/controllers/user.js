const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const randomstring = require('randomstring');
const nodemailer = require('nodemailer');

let secretToken;
const smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    secure: false, // use SSL
    port: 25, // port for secure SMTP
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
});

let mailOptions;

let sendVerifyToken = async (req, res, next) => {
    try {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        secretToken = randomstring.generate();
        mailOptions = {
            to: req.body.email,
            subject: "Potvrda naloga",
            html: "Zdravo,<br> Unesite naredni token kako biste završili registraciju.<br><p>" + secretToken + "</p>"
        };

        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                res.status(400).send(error);
                console.log(error);
            } else {
                res.status(200).send(response);
            }
        });
    } catch (error) {
        next(error);
    }
}

let sendMsg = async (req, res, next) => {
    try {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        secretToken = randomstring.generate();
        mailOptions = {
            to: process.env.EMAIL,
            subject: "Poruka od " + req.body.email,
            html: "Poruka:<br>" + req.body.msg + "<br>" + "Od:<br>" + req.body.email
        };

        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                res.status(400).send(error);
                console.log(error);
            } else {
                res.status(200).send(response);
            }
        });
    } catch (error) {
        next(error);
    }
}

let addNewUser = async (req, res, next) => {
    try {
        const newUser = {
            _id: new mongoose.Types.ObjectId,
            email: req.body.email,
            password: req.body.password,
            secretToken: '',
            active: false,
            admin: false
        };
        const user = await User.findOne({
            email: req.body.email
        });
        if (!user) {
            bcrypt.hash(req.body.password, 10, (_err, hash) => {
                newUser.password = hash;
                newUser.secretToken = secretToken;
                User.create(newUser)
                    .then(user => {
                        res.status(200).json({ msg: user.email + ' je registrovan!' });
                    })
                    .catch(err => {
                        res.send('error: ' + err);
                    })
            });
        } else {
            res.status(409).json({ error: "Korisnik vec postoji!" });
        }

    } catch (error) {
        next(error);
    }
}

let verify = async (req, res, next) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            return res.status(400).json({
                message: "Email ne postoji!"
            });
        }

        if (user.secretToken === req.body.secretToken) {
            await User.updateOne(
                { email: req.body.email },
                {
                    $set: { active: true }
                });

            const path = "/login/";
            return res.json({
                path: path
            });

        } else {
            return res.status(401).json({
                message: "Pogrešan verifikacioni kod!"
            });
        }

    } catch (error) {
        next(error);
    }
}

let listUsers = async (_req, res, next) => {
    try {
        const users = await User.find({});
        if (users.length === 0) {
            res.send("Nije pronadjen!").status(404);
            return;
        }
        else {
            res.json(users).status(200);
        }

    } catch (error) {
        next(error);
    }
}

let findUserByEmail = async (req, res, next) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });

        if (user) {
            res.status(200).json(user);
        } else {
            res.status(309).send("Korisnik sa ovim email-om ne postoji!");
        }
    } catch (error) {
        next(error);
    }
}

let findUser = async (req, res, next) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });
        if (user) {
            if (user.active) {
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    const payload = {
                        _id: user._id,
                        email: user.email
                    };

                    let token = jwt.sign(payload, process.env.SECRET_KEY, {
                        expiresIn: 1440
                    });

                    res.json({
                        token: token,
                        email: user.email,
                        username: user.username,
                        secretToken: user.secretToken
                    }).status(200);
                } else {
                    res.status(404).json({
                        error: "Uneti korisnik ne postoji!"
                    });
                }
            } else {
                res.status(404).json({
                    error: "Korisnik nije aktiviran!"
                });
            }
        } else {
            res.status(404).json({
                error: "Uneti korisnik ne postoji!"
            });
        }

    } catch (error) {
        next(error);
    }
}

let checkToken = async (req, res, next) => {
    try {
        const token = req.body.secretToken;
        if (token == null) {
            return res.sendStatus(401);
        }

        jwt.verify(token, process.env.SECRET_KEY, (err, user) => {
            if (err) {
                return res.status(403).send({ check: false })
            }
            res.status(200).json({
                user,
                check: true
            });
        });
    } catch (error) {
        next(error);
    }
}

module.exports = {
    addNewUser,
    sendVerifyToken,
    verify,
    findUser,
    findUserByEmail,
    listUsers,
    checkToken,
    sendMsg
}