const Service = require('../models/service');
const mongoose = require('mongoose');

let saveService = async (req, res, next) => {
    try {
        const size = req.body.size;
        const name = req.body.name;
        const city = req.body.city;
        const message = req.body.message;
        if (!size || !name || !city || !message) {
            res.status(400).send("Los zahtev!");
            return;
        }
        const newService = new Service({
            _id: new mongoose.Types.ObjectId,
            sizeOfservice: size,
            name,
            city,
            message,
            image: req.file.filename
        });
        await newService.save();
        res.status(201).send("Uspesno postavljena usluga!");
    } catch (error) {
        next(error);
    }
}

let getAllServices = async (_req, res, next) => {
    try {
        const services = await Service.find({});
        if (services) {
            res.status(200).json(services);
        } else {
            res.status(404).send("Nije pronadjen!");
        }
    } catch (error) {
        next(error);
    }
}

let findById = async (req, res, next) => {
    try {
        const service = await Service.findById(req.params.id);
        if (service) {
            res.status(200).json(service);
        } else {
            res.status(404).json("Nije pronadjen!");
        }
    } catch (error) {
        next(error);
    }
}

let findByName = async (req, res, next) => {
    try {
        const name = req.params.name;
        const service = await Service.find({
            name: new RegExp('.*' + name + '.*')
        });

        if (service.length > 0) {
            res.status(200).json(service);
        } else {
            res.status(201).json(service);
        }
    } catch (error) {
        next(error);
    }
}

let findByCity = async (req, res, next) => {
    try {
        const services = await Service.find({ city: req.params.city });
        if (services.length > 0) {
            res.status(200).json(services);
        } else {
            res.status(201).json(services);
        }
    } catch (error) {
        next(error);
    }
}

let listOfCities = async (_req, res, next) => {
    try {
        const cities = await Service.distinct('city');
        res.status(200).json(cities);
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllServices,
    findById,
    findByName,
    findByCity,
    saveService,
    listOfCities
}