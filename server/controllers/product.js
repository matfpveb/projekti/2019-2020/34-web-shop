const Product = require('../models/product');
const mongoose = require('mongoose');

let saveProduct = async (req, res, next) => {
    try {
        const size = req.body.size;
        const name = req.body.name;
        const city = req.body.city;
        const message = req.body.message;

        if (!size || !name || !city || !message) {
            res.status(400).send("Los zahtev!");
            return;
        }
        const newProduct = new Product({
            _id: new mongoose.Types.ObjectId,
            sizeOfProduct: size,
            name,
            city,
            message,
            image: req.file.filename
        });
        await newProduct.save();
        res.status(201).send("Uspesno postavljen proizvod!");
    } catch (error) {
        next(error);
    }
}

let getAllProducts = async (req, res, next) => {
    try {
        const product = await Product.find({});
        if (product) {
            res.status(200).json(product);
        } else {
            res.status(404).send("Nije pronadjen!");
        }
    } catch (error) {
        next(error);
    }
}

let findById = async (req, res, next) => {
    try {
        const product = await Product.findById(req.params.id);
        if (product) {
            res.status(200).json(product);
        } else {
            res.status(404).send("Nije pronadjen!");
        }
    } catch (error) {
        next(error);
    }
}

let findByName = async (req, res, next) => {
    try {
        const name = req.params.name;
        const product = await Product.find({
            name: new RegExp('.*' + name + '.*')
        });

        if (product.length > 0) {
            res.status(200).json(product);
        } else {
            res.status(201).json(product);
        }
    } catch (error) {
        next(error);
    }
}

let findByCity = async (req, res, next) => {
    try {
        const products = await Product.find({ city: req.params.city });
        if (products.length > 0) {
            res.status(200).json(products);
        } else {
            res.status(404).send("Nije pronadjen!");
        }
    } catch (error) {
        next(error);
    }
}

let listOfCities = async (_req, res, next) => {
    try {
        const cities = await Product.distinct('city');
        res.status(200).json(cities);
    } catch (error) {
        next(error);
    }
}

let mostCommonCity = async (_req, res, next) => {
    try {
        const cities = await Product.aggregate([{ $sortByCount: "$city" }]);
        res.status(200).json(cities);
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllProducts,
    findById,
    findByName,
    findByCity,
    saveProduct,
    listOfCities,
    mostCommonCity
}