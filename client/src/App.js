import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Ads from './components/Ads';
import AllProducts from './components/AllProducts';
import AllServices from './components/AllServices';
import Save from './components/Save';
import Register from './components/Register';
import Login from './components/Login';
import AdsByName from './components/AdsByName';
import AdsPerCity from './components/AdsPerCity';
import Verify from './components/Verify';
import WrapperComponent from './components/WrapperComponent';
import Contact from './components/Contact';
import UserAgrement from './components/UserAgrement';
import Privacy from './components/Privacy';

function PrivateRoute ({component: Component, ...rest}) {
  return (
    <Route {...rest} render={props => (
      <WrapperComponent {...props}/>
    )}/>
  )
}

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Ads} />
        <Route path="/allProducts" exact component={AllProducts} />
        <Route path="/contact" exact component={Contact} />
        <PrivateRoute path="/admin" exact component={Save} />
        <Route path="/ads/:name" exact component={AdsByName} />
        <Route path="/login" exact component={Login} />
        <Route path="/register" exact component={Register} />
        <Route path="/verify" exact component={Verify} />
        <Route path="/allServices" exact component={AllServices} />
        <Route path="/city/:city" exact component={AdsPerCity} />
        <Route path="/userAgrement" exact component={UserAgrement} />
        <Route path="/privacy" exact component={Privacy} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
