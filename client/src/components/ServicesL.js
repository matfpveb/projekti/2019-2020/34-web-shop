import React, { Component } from 'react';
import '../css/ServicesL.css'

class ServicesL extends Component {

  render() {
    return (
      <div className='ServicesL'>
        <div className='ServicesLContent'>
          <div className="ServicesLBox">
            <img src={this.props.image} alt="ProductPhoto" />
            <h3> {this.props.name} </h3>
            <p> {this.props.price} </p>
            <p> {this.props.message} </p>
          </div>
          <p id="dateOfPublishing"> {this.props.dateOfCreateProduct} </p>
        </div>
      </div>
    );
  }
}

export default ServicesL;