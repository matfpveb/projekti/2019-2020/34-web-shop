import React, { Component } from 'react';
import '../css/Save.css';
import SaveObject from './SaveObject';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

class Save extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disabledProduct: true,
            disabledService: false
        };
  
        this.showFormForProduct = this.showFormForProduct.bind(this);
        this.showFormForService = this.showFormForService.bind(this);
    }

    showFormForProduct() {
        this.setState({
            disabledProduct: true,
            disabledService: false
        });
    }

    showFormForService() {
        this.setState({
            disabledProduct: false,
            disabledService: true
        });
    }

    render() {
        let typeOfProduct;
        if (this.state.disabledProduct) {
            typeOfProduct = <SaveObject type="product" />
        } else if (this.state.disabledService){
            typeOfProduct = <SaveObject type="services" />
        } 

        return (
        <div className="save">
            <a href='/' className="back"><FontAwesomeIcon icon={faAngleLeft} className="fi_menu" /> Vratite se na početnu</a>

            <h1 id="titleAdmin">ADMIN PANEL</h1>
            <div className="buttonsInAdminPanel">
                <button type="button" disabled={this.state.disabledProduct} onClick={this.showFormForProduct} id="btnProduct" className="btn btn-success">Oglasite proizvod</button>
                <button type="button" disabled={this.state.disabledService} onClick={this.showFormForService} id="btnCommemoration" className="btn btn-success">Oglasite uslugu</button>
            </div>
            {typeOfProduct}
        </div>
    );
  }
}

export default Save;