import React, { Component } from 'react';
import '../css/Footer.css'

class Footer extends Component {

    render() {
        return (
            <div className="footer">
                <div className="footer_top">
                    <div className="footer_info">
                        <div className="first">
                            <h4>WEBSHOP.RS - KUPI BRZO,PRODAJ LAKO</h4>
                            <p> Prvi srpski portal na kojem se svakog dana objavljuju oglasi sa proizvodima
                            i uslugama iz cele Srbije.Naš cilj je da olakšamo poslovanje i spojimo ljude.
                            Na brz lak i jednostavan nacin</p>
                        </div>
                        <div className="second">
                            <h4>MAPA SAJTA</h4>
                            <ul>
                                <li> <a href="/#"> SVI OGLASI </a> </li>
                                <li> <a href="/allProducts"> PROIZVODI </a> </li>
                                <li> <a href="/allServices"> USLUGE </a> </li>
                                <li> <a href="/contact"> KONTAKT </a> </li>
                            </ul>
                        </div>
                        <div className="third">
                            <h4>PLATITE ONLINE KARTICOM</h4>
                            <p>Online kupovina, uksoro...</p>
                            <div>
                                <img id='debitcard' src="/mastercard.png" alt="mastercard_photo" />
                                <img id='debitcard' src="/maestro.png" alt="maestro_photo" />
                                <img id='debitcard' src="/visa.gif" alt="visa_photo" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer_down">
                    <div className="footer_down_copyright">
                        <p>© Copyright <a href="/">webshop.rs</a></p>
                    </div>
                    <div className="footer_down_img">
                        <img src="/logo.png" alt="visa_photo" />
                    </div>
                    <div className="footer_down_links">
                        <a href="/userAgrement">Uslovi korišćenja</a>
                        <a href="/privacy" className="footer_down_link_2">Politika privatnosti</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;