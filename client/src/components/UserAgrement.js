import React from 'react';
import './../App.css';
import './../css/UserAgrement.css';
import Header from './Header';
import Footer from './Footer';

class UserAgrement extends React.Component {

    render() {
        return (
            <div>
                <Header />
                <div className="userAgrementContainer">
                    <p>
                        <h3>Pravila i uslovi korišćenja WebShop</h3>
                        <span>(Datum poslednje izmene: 18.5.2020. godine)</span> <br /><br />
                        Ova Pravila i uslovi imaju svojstvo Ugovora između:<br /><br />
                        1.Kompanije Quable B.V., osnivača i vlasnika WebShop, sa sedištem u Plesmanlaan 84, 2497CB, Hag, Holandija, registrovane kod Privredne komore Holandije pod brojem KvK 27302667 i sa BTW (PDV) brojem: 820056352B01 (u daljem tekstu Quable B.V.) i<br />
                        2.Svakog pojedinačnog korisnika internet portala i mobilne aplikacije WebShop (u daljem tekstu Korisnik usluga) (u daljem tekstu zajedno označeni kao Ugovorne strane)<br /><br />
                        Dobrovoljnim korišćenjem bilo kojeg segmenta WebShop na bilo koji način, svaki Korisnik usluga potvrđuje da je upoznat i saglasan sa tekstom ovih Pravila i uslova (Ugovora). Ugovor se zaključuje na srpskom jeziku.<br /><br />

                        <h4>1. Obaveze Ugovornih strana</h4> <br /><br />
                        Quable B.V. se obavezuje da će Korisnicima usluga pružati usluge definisane ovim Pravilima i uslovima, na način i sa ograničenjima opisanim u ovim Pravilima i uslovima, a u skladu sa važećim propisima Republike Srbije. Korisnik usluga se obavezuje da će prilikom korišćenja WebShop u svemu poštovati ova Pravila i uslove. Ugovorne strane su saglasne da je zarad ispunjenja navedenih obaveza neophodno da Quable B.V. prikuplja, obrađuje i čuva lične podatke Korisnika usluga u skladu sa važećim propisima Republike Srbije i Polisom o poštovanju privatnosti koja je dostupna na uvid svim Korisnicima usluga. Prihvatanjem ovih Pravila i uslova korišćenja WebShop, svaki Korisnik usluga potvrđuje da je upoznat i saglasan sa Polisom o poštovanju privatnosti, te daje izričiti pristanak za prikupljanje i obradu podataka kakva je neophodna zarad ispunjavanja navedenih obaveza od strane Quable B.V.<br /><br />

                        <h4>2. Pružalac usluga informacionog društva</h4><br /><br />
                        Internet portal i mobilne aplikacije WebShop, u vlasništvu kompanije Quable B.V., zajedno su klasifikovani internet oglasnik - pružalac usluga informacionog društva (član 3. Zakona o elektronskoj trgovini Republike Srbije, "Sl. glasnik RS", br. 52/2019), u daljem tekstu WebShop, a koji Korisnicima usluga pruža besplatne i plaćene usluge objavljivanja i promocije oglasa na internetu.<br /><br />

                        <h4>3. Besplatne usluge</h4> <br /><br />
                        WebShop pruža usluge besplatnog klasifikovanog objavljivanja oglasa, u skladu sa definicijama iz člana 2. Zakona o oglašavanju Republike Srbije i člana 3. stav 6. Zakona o elektronskoj trgovini Republike Srbije.<br /><br />

                        <h4>4. Plaćene usluge</h4><br /><br />
                        WebShop pruža plaćene usluge: a) promocije besplatno postavljenih oglasa, a sa ciljem povećanja njihove vidljivosti i posećenosti; b) usluge reklamiranja putem banera na portalu i mobilnoj aplikaciji; c) usluge prodajnih WS izloga. WebShop zadržava pravo da bez najave menja cene svojih plaćenih usluga, u skladu sa svojom poslovnom politikom.<br /><br />

                        <h4>5. Registracija i oglašavanje od strane Korisnika usluga</h4><br /><br />
                        Za korišćenje usluga WebShop neophodna je registracija Korisnika usluga (otvaranje korisničkog naloga), koja je besplatna. Registracija se obavlja prijavom na sistem WebShop tako što Korisnik usluga unese e-mail adresu i lozinku koju će koristiti na WebShop, a zatim potvrdi registraciju putem linka u registracionom e-mailu koji je dobio od WebShop. Takođe, moguća je i registracija i logovanje putem društvene mreže Facebook, kao i preko naloga na Googleu i Appleu (social login). Registrovani i ulogovani Korisnik usluga može postavljati oglase, i/ili koristiti elektronski sistem za razmenu poruka unutar WebShop zarad komunikacije sa drugim Korisnicima usluga. Korisnik usluga može imati samo jedan registrovani korisnički nalog. Registrovani Korisnik usluga koji je postavio oglas, ima sva prava i obaveze proistekle iz Zakona o oglašavanju Republike Srbije. Registrovani Korisnik usluga prihvata mogućnost kontakta na e-mail ili na broj telefona (ukoliko je taj podatak ostavio na WebShop), a u cilju realizacije željene prodaje ili kupovine koja je bila svrha postavljanja oglasa, kao i u cilju kontakta sa administracijom WebShop radi pomoći i/ili odgovora na pitanja. Korisnik usluga prihvata da će sadržaj njegovog oglasa, odnosno tekst oglasa, fotografije, kao i njegov kontakt telefon, biti objavljeni na internet stranicama WebShop, po mogućstvu i na Facebook stranici WebShop, te da će kao takav biti javno dostupan svim korisnicima interneta. Oglasi koje postavljaju Korisnici usluga, uključujući i one za koje su aktivirane usluge plaćene promocije, moraju u svemu biti u skladu sa Zakonom o oglašavanju Republike Srbije i Zakonom o autorskom i srodnim pravima. Svaki oglas koji nije u skladu sa Zakonom o oglašavanju i Zakonom o autorskim i srodnim pravima biće obrisan sa WebShop, uz obaveštenje Korisniku usluga koji je takav lični oglas postavio.<br /><br />

                        <h4>6. Ograničenja prilikom korišćenja WebShop</h4><br /><br />
                        Korisnici usluga su saglasni da će prilikom korišćenja WebShop poštovati sledeća ograničenja:<br />
                        Nije dozvoljeno postavljati oglase koji su na bilo koji način u suprotnosti sa odredbama Zakona o oglašavanju Republike Srbije.<br />
                        Nije dozvoljeno postavljati oglase koji sadrže neistine i laži, koji mogu dovesti bilo koga u zabludu, koji vređaju bilo koga, bilo kome nanose štetu, te koriste bilo čiju lakovernost.<br />
                        Nije dozvoljeno postavljati oglase kojima se oglašavaju: piratske kopije proizvoda, internet nalozi, digitalna distribucija i nabavka igara za fizička lica i druga virtualna dobra (kriptovalute i sl.), pirotehnička sredstva, medicinski ili medicinsko-alternativni postupci i metode, nadrilekarstvo, opojne droge i njihova upotreba, seksualna pomagala, oružje i municija, uključujući kopije, startne pištolje i vazdušno oružje, zatim duvan i duvanski proizvodi (uključujući i elektronske cigarete) i njihovo konzumiranje, te alkoholna pića (sem piva i vina) i njihovo konzumiranje.<br />
                        Nije dozvoljeno postavljati oglase u kojima se nudi upoznavanje ili posredovanje u upoznavanju radi sklapanja braka ili veze.<br />
                        Nije dozvoljeno postavljati oglase sa političkim sadržajem.<br />
                        Kroz oglase nije dozvoljeno oglašavanje prostitucije i pornografskih sadržaja, kao i igara na sreću.<br />
                        Kroz oglase nije dozvoljeno oglašavanje preprodaje i kupovine ulaznica za bioskope, sportske i druge priredbe.<br />
                        Kroz oglase nije dozvoljeno oglašavanje u formi javnog obraćanja ili proglasa.<br />
                        Kroz oglase nije dozvoljeno oglašavanje lekova, lekovitih preparata i suplemenata.<br />
                        Kroz oglase nije dozvoljeno oglašavanje sredstava za samoodbranu (sprejevi, elektrošokeri, palice, bokseri i slično).<br />
                        Fizičkim licima nije dozvoljeno da kroz lične oglase oglašavaju potpuno nove stvari, s obzirom na to da je za trgovnu potpuno novim stvarima neophodna registracija pravnog lica ili preduzetnika ("svojstvo trgovca" po Zakonu o trgovini).<br />
                        Nije dozvoljeno postavljanje oglasa čiji predmet oglašavanja sadrži tuđe lično ime i prezime, osim u slučaju da za to oglašivač ima pribavljenu izričitu saglasnost nosioca tog ličnog imena i prezimena (npr. lično ime u nazivu oglašenog domena, naslovu ili opisu oglasa i slično).<br /><br />
                        <h4>7. Nedozvoljene radnje prilikom postavljanja oglasa na WebShop</h4><br /><br />
                        Korisnici usluga su saglasni da će prilikom postavljanja oglasa na WebShop poštovati sledeća ograničenja:<br />
                        Nije dozvoljeno više puta postaviti identičan ili sličan oglas (tzv. “spamovanje”). Slični oglasi su oni koji imaju isti predmet prodaje. Takođe, nije dozvoljeno postavljati identične oglase sa različitih naloga.<br />
                        Nije dozvoljeno ubacivanje URL linka firme, internet prodavnice ili drugog sajta (pogotovo konkurentskog ili sajta koji promoviše "zaradu preko interneta") u tekst (opis) oglasa.<br />
                        Nije dozvoljeno postavljanje fotografija koje sadrže grafičku obradu radi posebnog isticanja, ili sadrže logotip firmi i drugih internet portala. Na fotografijama koje se postavljaju uz oglas nije dozvoljno prikazati lica osoba i detalje koji mogu dovesti do identifikacije osoba.<br />
                        Nije dozvoljeno unositi specijalne grafičke karaktere u naslov ili opis oglasa sa ciljem isticanja oglasa.<br />
                        Nije dozvoljeno u tekst oglasa unositi tekstove koji nisu u vezi sa predmetom oglašavanja (npr. komentari, poezija, citati radi bolje posećenosti i sl).<br />
                        Nije dozvoljeno unositi tagove (reči za pretragu) u opis oglasa.<br />
                        Nije dozvoljeno učestalo postavljanje i brisanje oglasa radi dominacije na prvim stranicama grupa ili kategorija - pravilo fer korišćenja besplatnih usluga WebShop. Učestalim ponavljanjem se smatra češće od jednom u tri dana.<br />
                        Nije dozvoljeno namerno postavljanje oglasa u pogrešnim odnosno neprikladnim kategorijama i grupama.<br /><br />
                        <h4>8. Pravila i uslovi za slanje poruka unutar WebShop</h4><br /><br />
                        a)Registrovanim Korisnicima usluga, WebShop nudi uslugu besplatne komunikacije putem sistema za razmenu poruka unutar WebShop. Komunikacija putem ovih poruka je slobodna, pod uslovom poštovanja sledećih ograničenja:<br />
                        b)Nije dozvoljeno koristiti WebShop za slanje uvredljivih poruka drugim Korisnicima usluga.<br />
                        c)Nije dozvoljeno slanje spam poruka drugim Korisnicima usluga. Spam poruka je više puta ponovljena poruka, koja nije u vezi sa ličnim oglasom oglašivača.<br />
                        d)Nije dozvoljeno korišćenje (umetanje) URL-ova ka oglasima ili stranicama na WebShop u spam e-mail porukama ka drugim korisnicima interneta, tj. ka bilo kojim korisnicima interneta koji nisu unapred dali odobrenje za primanje takvih e-mail poruka.<br />
                        e)Nije dozvoljeno slanje poruka u kojima se direktno ili indirektno promovišu drugi portali.<br /><br />
                        <h4>9. Pravila i uslovi za ocenjivanje korisnika na WebShop</h4><br /><br />
                        Korisnika možete oceniti jedino ako ste sa njim imali obostranu razmenu poruka (obe strane su poslale po najmanje jednu poruku) preko sistema za razmenu poruka unutar WebShop (nadalje WS poruka).<br />
                        Da biste mogli oceniti drugog korisnika, od poslednje razmene WS poruka sa njim ne sme proći više od 30 dana.<br />
                        Ako je od poslednje razmene WS poruka prošlo više od 30 dana, neće postojati mogućnost ocenjivanja.<br />
                        Korisnika kojeg ste jednom ocenili iz jedne razmene WS poruka ne možete više oceniti iz iste razmene WS poruka.<br />
                        Da biste mogli istog korisnika oceniti ponovo, neophodno je da prođe punih 7 dana od poslednjeg ocenjivanja.<br />
                        Nije dozvoljeno davanje ili prikupljanje neosnovanih ocena.<br />
                        Nije dozvoljeno unošenje uvredljivog sadržaja u opis ocene ili odgovor na ocenu.<br />
                        U opis ocene ili odgovor na ocenu ne smeju se unositi lični podaci korisnika poput JMBG-a, broja lične karte, adrese stanovanja, registracionih podataka pravnog lica.<br /><br />
                        <h4>10. Trgovina između Korisnika usluga</h4><br /><br />
                        WebShop ne posreduje u trgovini između Korisnika usluga. Svaka trgovina ili razmena između Korisnika usluga WebShop je na sopstveni rizik Korisnika usluga. WebShop, niti njegov vlasnik Quable B.V., neće snositi nikakvu odgovornost za eventualnu štetu nastalu prilikom bilo kakvih transakcija između Korisnika usluga. Korisnici usluga se u svakom slučaju pozivaju na oprez prilikom međusobne trgovine.<br /><br />

                        <h4>11. Autorska prava i zabrana neovlašćenog kopiranja i korišćenja sadržaja objavljenih na WebShop</h4><br /><br />
                        U skladu sa Zakonom o autorskom i srodnim pravima Republike Srbije, nije dozvoljeno bilo kakvo, ručno ili softversko, neovlašćeno kopiranje i skrepovanje sadržaja ili delova sadržaja objavljenih na WebShop. Pod sadržajem se podrazumevaju tekstualni sadržaji stranica, oglasi, podaci o Korisnicima usluga, zanimljivosti, fotografije, logotipi, grafička rešenja i svi ostali sadržaji koji se po Zakonu o autorskom i srodnim pravima smatraju autorskim delom. Quable B.V. zadržava pravo da prekršioca goni sudski. Korisnik usluga zadržava autorsko pravo nad pojedinačnim tekstom i fotografijama svojih oglasa. Korisnik usluga pristaje na umetanje vodenog žiga od strane WebShop u svaku od fotografija koju priloži prilikom postavljanja oglasa, kao i na obradu dimenzija fotografija radi njihovog estetskog uklapanja u izgled stranica na WebShop.<br /><br />

                        <h4>12. Prikupljanje i obrada podataka, uključujući podatke o ličnosti</h4><br /><br />
                        Prikupljanje, obrada i čuvanje podataka od strane WebShop se obavlja u skladu sa važećim propisima Republike Srbije i Polisom o poštovanju privatnosti koja je dostupna na uvid svim Korisnicima usluga. Prihvatanjem ovih Pravila i uslova korišćenja WebShop, svaki Korisnik usluga potvrđuje da je upoznat i saglasan sa Polisom o poštovanju privatnosti. WebShop od svojih registrovanih Korisnika usluga prikuplja lične podatke (podatke o ličnosti), odnosno podatke o firmi ako su u pitanju pravna lica. Prikupljanje podataka o IP adresi korisnika radi identifikacije zakonska je obaveza po članu 16. Zakona o elektronskoj trgovini. Sve druge svrhe prikupljanja podataka su izlistane i pojašnjene u Polisi o poštovanju privatnosti. Način obrade i čuvanja podataka, kao i trajanje čuvanja podataka, pojašnjeno je u Polisi o poštovanju privatnosti. Quable B.V. može dati pristup ličnim podacima Korisnika usluga samo licima sa kojima ima ugovorni ili radni odnos, bilo direktno bilo preko ovlašćenog posrednika. Korisnici usluga treba da budu svesni da će određeni podaci o ličnosti, tj. oni podaci koji su neizostavni deo oglasa postavljenog na WebShop od strane Korisnika usluga, biti dostupni drugim Korisnicima usluga, odnosno biti javno dostupni na internetu. Registrovani Korisnik usluga prihvata mogućnost kontakta putem e-maila ili SMS-a od strane tima WebShop u svrhu unapređenja i razvoja sajta WebShop, kao i u promotivne ili marketinške svrhe. Korisnik usluga ima pravo da opozove pristanak koji je prihvatanjem ovih Pravila i uslova dao za prikupljanje i obradu ličnih podataka, kao i pravo da zatraži izmenu i brisanje ličnih podataka. O ovim i drugim pravima Korisnika usluga u vezi sa ličnim podacima, kao i o načinu ostvarivanja tih prava, Korisnik usluga se uvek može informisati u Polisi o poštovanju privatnosti. U slučaju da zbog brisanja podataka dođe do nepostojanja uslova za dalje pružanje usluga, Korisniku usluga može biti uskraćen pristup delovima ili celom WebShop. Quable B.V. ne odgovara za tačnost podataka koje su na WebShop unela druga lica, kao ni za tačnost rezultata obrade takvih podataka.<br /><br />

                        <h4>13. Kršenja ovih Pravila i uslova korišćenja WebShop</h4><br /><br />
                        Za bilo koje kršenje Pravila i uslova korišćenja WebShop, nepoštovanje u ovom tekstu navedenih ograničenja ili kršenje zakona od strane Korisnika usluga, moguće je izricanje opomene Korisniku usluga, odnosno privremena ili trajna blokada korisničkog naloga Korisnika usluga. Po pravilu, a u zavisnosti od težine i učestalosti kršenja, administracija WebShop najpre izriče meru opomene, a zatim donosi meru privremene ili trajne blokade korisničkog naloga. Korisnici usluga čiji je nalog privremeno ili trajno blokiran nemaju pravo ni na kakvu nadokandu štete zbog toga što za vreme trajanja blokade naloga nisu mogli da koriste usluge WebShop. Navedene mere se mogu primeniti i u slučajevima kada nije došlo do kršenja Pravila i uslova, i to u cilju sprečavanja nastajanja štete, ili u slučajevima kada usled određenog činjenja ili nečinjenja od strane Korisnika usluga dolazi do otežavanja poslovne aktivnosti Quable B.V. ili otežavanja rada WebShop.<br /><br />

                        <h4>14. Isključenje odgovornosti</h4><br /><br />
                        WebShop isključuje sopstvenu odgovornost u skladu sa sledećim članovima Zakona o elektronskoj trgovini Republike Srbije:<br />
                         1)U skladu sa članom 16. Zakona o elektronskoj trgovini Republike Srbije, WebShop nije odgovoran za sadržaj elektronskih poruka koje korisnici razmenjuju koristeći sistem za razmenu poruka unutar WebShop.<br />
                         2)U skladu sa članovima 17. i 18. Zakona o elektronskoj trgovini Republike Srbije, WebShop nije odgovoran za sadržaj podataka skladištenih u bazi podataka i/ili objavljenih na WebShop.<br />
                         3)U skladu sa članom 19. Zakona o elektronskoj trgovini Republike Srbije, WebShop nije odgovoran za sadržaj na drugim portalima u slučaju kada Korisnici usluga u svojim oglasima ili porukama umeću linkove koji vode ka tim drugim portalima.<br />
                         4)U skladu sa članom 20. Zakona o elektronskoj trgovini Republike Srbije, WebShop nije dužan da pregleda podatke koje je skladištio, preneo ili učinio dostupnim, odnosno da ispituje okolnosti koje bi upućivale na nedopustivo delovanje korisnika usluga.<br /><br />
                        <h4>15. Isključenje odgovornosti u slučaju nedostupnosti i prestanka emitovanja oglasa</h4><br /><br />
                        Iz razloga više sile ili tehničkih problema, moguće je da WebShop nije dostupan svima ili nekom delu korisnika u toku određenih vremenskih perioda. U takvim slučajevima Quable B.V. ne odgovara za eventualnu štetu nastalu zbog prestanka emitovanja ličnih oglasa. Quable B.V. zadržava pravo da iz bilo kog razloga odbije objavljivanje oglasa, i u tom slučaju ne odgovara za eventulanu štetu nastalu ovim činjenjem.<br /><br />

                        <h4>16. Kontakt sa korisničkim timom i zabrana objavljivanja prepiske</h4><br /><br />
                        Za kontakt sa korisničkim timom WebShop (administracijom WebShop) korisnici se upućuju na kontakt formu. Kada WebShop otvaraju sa računara, korisnici kontakt formu mogu pronaći u donjem desnom uglu (kružnog je oblika, sa tekstom “Predlog, Problem, Kontakt”), a ukoliko WebShop otvaraju preko mobilnog telefona, kontakt forma im je dostupna u meniju “Moj WS” (opcija “Pomoć i kontakt”). Odgovor administratora ili korisničkog tima WebShop ne mora da odražava stanovište kompanije Quable B.V., pa u skladu sa tim Korisnici usluga ne mogu ostvariti nikakva prava na osnovu takve pisane prepiske. Nije dozvoljeno bilo kakvo objavljivanje pisane prepiske korisničkog tima WebShop sa Korisnikom usluga bez pisane dozvole Quable B.V. ili njegovog ovlašćenog lica.<br /><br />

                        <h4>17. Zaštita privatnosti</h4> <br /><br />
                        Prikupljanje, obrada i čuvanje ličnih podataka od strane WebShop se obavlja u skladu sa važećim propisima Republike Srbije i Polisom o poštovanju privatnosti koja je dostupna na uvid svim Korisnicima usluga. Prihvatanjem ovih Pravila i uslova korišćenja WebShop, svaki Korisnik usluga potvrđuje da je upoznat i saglasan sa Polisom o poštovanju privatnosti.<br /><br />

                        <h4>18. WS izlog</h4><br /><br />
                        WebShop nudi mogućnost zakupa prodajnog internet izloga (u daljem tekstu WS izlog) u kome se prikazuju svi oglasi korisnika usluga. WS izlog će biti dostupan na jedinstvenoj internet adresi (domenu) po modelu: IME-IZLOGA.WSizlog.rs, odnosno IME-IZLOGA.WSizlog.com. Parametar IME-IZLOGA bira korisnik prilikom aktivacije izloga. Domeni WSizlog.rs i WSizlog.com, kao i svi njihovi poddomeni dodeljeni WS izlozima, ostaju u vlasništvu Quable B.V. i Korisnik usluga ne polaže nikakva vlasnička prava nad njima. Korišćenjem usluge WS izlog, Korisnik usluge u svemu prihvata ova Pravila i uslove korišćenja WebShop. Prilikom aktivacije WS izloga, zabranjeno je koristiti zaštićena imena. Nakon isteka WS izloga, internet adresa izloga ostaje rezervisana 30 dana, posle kojeg perioda se ona briše iz baze i slobodna je drugim Korisnicima usluga za zakup. U slučaju trajnog blokiranja korisničkog naloga, blokira se i pripadajući WS izlog. Iz razloga više sile ili tehničkih problema, WS izlog može biti nedostupan. Korisnik usluga ni u kom slučaju ne može od Quable B.V. zahtevati bilo kakvu odštetu zbog nedostupnosti WS izloga. Quable B.V. zadržava pravo da korisnički WS izlog ukine ili izmeni kako bi obezbedio poštovanje ovih Pravila i uslova, ili kako bi unapređivao funkcionalnosti izloga.<br /><br />

                        <h4>19. Izmene ovih Pravila i uslova</h4><br /><br />
                        Ova Pravila i uslovi su podložni promenama. U slučaju značajnih izmena, Quable B.V. zadržava pravo da od korisnika zatraži ponovno prihvatanje Pravila i uslova za korišćenje WebShop, te da uskrati pristup WebShop korisnicima koji ne prihvate promenjena Pravila i uslove.<br /><br />

                        <h4>18. 5. 2020. godine</h4><br /><br />
                        Hag, Holandija
                    </p>
                </div>
                <Footer />
            </div>
        );
    }
}

export default UserAgrement;
