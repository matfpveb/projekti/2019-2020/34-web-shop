import React, { Component } from 'react';
import axios from 'axios';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import '../css/SaveObject.css';
import ProductMSimplified from './ProductM';
import ProductLSimplified from './ProductL';
import ServicesLSimplified from './ServicesL';
import ServicesMSimplified from './ServicesM';


class SaveObject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            size: 'm',
            message: '',
            image: null,
            imageForShow: null,
            city: 'Ada',
            name: '',
            object: null
        };

        this.onImageChange = this.onImageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showObject = this.showObject.bind(this);
        this.handleSize = this.handleSize.bind(this);
        this.handleCity = this.handleCity.bind(this);
    }

    componentDidMount() {
        this.setState({
            size: 'm',
            message: '',
            image: null,
            imageForShow: null,
            city: 'Ada',
            name: '',
            object: null
        });
    }

    handleChange(nameOfInput, event) {
        if (nameOfInput === 'name') {
            this.setState({ name: event.target.value });
        } else if (nameOfInput === 'message') {
            this.setState({ message: event.target.value });
        }
    }

    onImageChange(event) {
        let file = event.target.files[0];
        this.setState({
            image: file,
            imageForShow: URL.createObjectURL(event.target.files[0])
        });
    }

    handleSize(size) {
        this.setState({
            size: size.target.value
        });
    }

    handleCity(city) {
        this.setState({
            city: city.target.value
        });
    }

    showObject() {
        let submitButton = document.getElementById("submit");
        submitButton.style.display = "block";
        let object = '';
        let date = new Date();

        if (this.props.type === "product") {
            if (this.state.size === "m") {
                object = <ProductMSimplified name={this.state.name}
                    image={this.state.imageForShow}
                    message={this.state.message}
                    dateOfCreateProduct={moment(date).format('DD.MM.YYYY')} />;
            } else if (this.state.size === "l") {
                object = <ProductLSimplified name={this.state.name}
                    image={this.state.imageForShow}
                    message={this.state.message}
                    dateOfCreateProduct={moment(date).format('DD.MM.YYYY')} />;
            }
        } else if (this.props.type === "services") {
            if (this.state.size === "m") {
                object = <ServicesMSimplified name={this.state.name}
                    image={this.state.imageForShow}
                    message={this.state.message}
                    dateOfCreateProduct={moment(date).format('DD.MM.YYYY')} />;
            } else if (this.state.size === "l") {
                object = <ServicesLSimplified name={this.state.name}
                    image={this.state.imageForShow}
                    message={this.state.message}
                    dateOfCreateProduct={moment(date).format('DD.MM.YYYY')} />;
            }
        }
        this.setState({ object });
    }
    handleSubmit(event) {
        let data = new FormData();
        data.append('image', this.state.image);
        data.append('name', this.state.name);
        data.append('city', this.state.city);
        data.append('message', this.state.message);
        data.append('size', this.state.size);
        if (this.props.type === "services") {
            axios.post('http://localhost:3001/service/save', data)
                .then(response => window.alert(response.data));
        }
        else {
            axios.post('http://localhost:3001/product/save', data)
                .then(response => window.alert(response.data));
        }
    }

    render() {
        let message = '';
        let size = '';
        let city = '';
        let title = '';
        let object = this.state.object;
        if (this.props.type === "services") {
            title = "Usluga";

        } else {
            title = "Proizvod";
        }
        message =
            <div className="form-group">
                <label htmlFor="message">Poruka:</label>
                <input type="text" className="form-control" placeholder="Unesite poruku" name="message" onChange={this.handleChange.bind(this, "message")} />
            </div>;
        size =
            <div className="form-group">
                <label htmlFor="size">Veličina oglasa:</label>
                <select className="browser-default custom-select" name="size" id="size" onChange={this.handleSize.bind(this)}>
                    <option value="m">M</option>
                    <option value="l">L</option>
                </select>
            </div>;
        city =
            <div className="form-group">
                <label htmlFor="city">Grad:</label>
                <select className="browser-default custom-select" name="city" id="city" onChange={this.handleCity.bind(this)}>
                    <option value="Ada">Ada </option>
                    <option value="Aleksandrovac">Aleksandrovac </option>
                    <option value="Aleksinac">Aleksinac </option>
                    <option value="Alibunar">Alibunar </option>
                    <option value="Apatin">Apatin </option>
                    <option value="Aranđelovac">Aranđelovac </option>
                    <option value="Arilje">Arilje </option>
                    <option value="Babušnica">Babušnica </option>
                    <option value="Bajina Bašta">Bajina Bašta </option>
                    <option value="Bajmok">Bajmok </option>
                    <option value="Banatsko  Novo Selo">Banatsko Novo Selo </option>
                    <option value="Barajevo">Barajevo </option>
                    <option value="Barič">Barič </option>
                    <option value="Batočina">Batočina </option>
                    <option value="Bač">Bač </option>
                    <option value="Bačka Palanka">Bačka Palanka </option>
                    <option value="Bačka Topola">Bačka Topola </option>
                    <option value="Bački Jarak">Bački Jarak </option>
                    <option value="Bački Petrovac">Bački Petrovac </option>
                    <option value="Bačko Petrovo">Bačko Petrovo </option>
                    <option value="Bačko Petrovo Selo">Bačko Petrovo Selo </option>
                    <option value="Bela Crkva">Bela Crkva </option>
                    <option value="Bela Palanka">Bela Palanka </option>
                    <option value="Beograd">Beograd </option>
                    <option value="Beočin">Beočin </option>
                    <option value="Bečej">Bečej </option>
                    <option value="Beška">Beška </option>
                    <option value="Blace">Blace </option>
                    <option value="Bogatić">Bogatić </option>
                    <option value="Bojnik">Bojnik </option>
                    <option value="Boljevac">Boljevac </option>
                    <option value="Bor">Bor </option>
                    <option value="Bosilegrad">Bosilegrad </option>
                    <option value="Brus">Brus </option>
                    <option value="Bujanovac">Bujanovac </option>
                    <option value="Crepaja">Crepaja </option>
                    <option value="Crvenka">Crvenka </option>
                    <option value="Despotovac">Despotovac </option>
                    <option value="Dimitrovgrad">Dimitrovgrad </option>
                    <option value="Doljevac">Doljevac </option>
                    <option value="Dolovo">Dolovo </option>
                    <option value="Futog">Futog </option>
                    <option value="Golubac">Golubac </option>
                    <option value="Gornji Milanovac">Gornji Milanovac </option>
                    <option value="Grocka">Grocka </option>
                    <option value="Inđija">Inđija </option>
                    <option value="Irig">Irig </option>
                    <option value="Ivanjica">Ivanjica </option>
                    <option value="Jagodina">Jagodina </option>
                    <option value="Kanjiža">Kanjiža </option>
                    <option value="Kać">Kać </option>
                    <option value="Kikinda">Kikinda </option>
                    <option value="Kladovo">Kladovo </option>
                    <option value="Knić">Knić </option>
                    <option value="Knjaževac">Knjaževac </option>
                    <option value="Koceljeva">Koceljeva </option>
                    <option value="Kosjerić">Kosjerić </option>
                    <option value="Kosovska Mitrovica">Kosovska Mitrovica </option>
                    <option value="Kostolac">Kostolac </option>
                    <option value="Kovačica">Kovačica </option>
                    <option value="Kovilj">Kovilj </option>
                    <option value="Kovin">Kovin </option>
                    <option value="Kragujevac">Kragujevac </option>
                    <option value="Kraljevo">Kraljevo </option>
                    <option value="Krupanj">Krupanj </option>
                    <option value="Kruševac">Kruševac </option>
                    <option value="Kula">Kula </option>
                    <option value="Kupinovo">Kupinovo </option>
                    <option value="Kuršumlija">Kuršumlija </option>
                    <option value="Kučevo">Kučevo </option>
                    <option value="Lajkovac">Lajkovac </option>
                    <option value="Lapovo">Lapovo </option>
                    <option value="Lazarevac">Lazarevac </option>
                    <option value="Lebane">Lebane </option>
                    <option value="Leposavić">Leposavić </option>
                    <option value="Leskovac">Leskovac </option>
                    <option value="Ljig">Ljig </option>
                    <option value="Ljubovija">Ljubovija </option>
                    <option value="Loznica">Loznica </option>
                    <option value="Majdanpek">Majdanpek </option>
                    <option value="Majur">Majur </option>
                    <option value="Mali Iđoš">Mali Iđoš </option>
                    <option value="Mali Zvornik">Mali Zvornik </option>
                    <option value="Mionica">Mionica </option>
                    <option value="Mladenovac">Mladenovac </option>
                    <option value="Mol">Mol </option>
                    <option value="Negotin">Negotin </option>
                    <option value="Niš">Niš </option>
                    <option value="Niška">Niška Banja </option>
                    <option value="Nova Crnja">Nova Crnja </option>
                    <option value="Nova Pazova">Nova Pazova </option>
                    <option value="Nova Varoš">Nova Varoš </option>
                    <option value="Novi Banovci">Novi Banovci </option>
                    <option value="Novi Bečej">Novi Bečej </option>
                    <option value="Novi Kneževac">Novi Kneževac </option>
                    <option value="Novi Pazar">Novi Pazar </option>
                    <option value="Novi Sad">Novi Sad </option>
                    <option value="Obrenovac">Obrenovac </option>
                    <option value="Odžaci">Odžaci </option>
                    <option value="Opovo">Opovo </option>
                    <option value="Pančevo">Pančevo </option>
                    <option value="Paraćin">Paraćin </option>
                    <option value="Petrovac">Petrovac </option>
                    <option value="Petrovac na Mlavi">Petrovac na Mlavi </option>
                    <option value="Petrovaradin">Petrovaradin </option>
                    <option value="Pećinci">Pećinci </option>
                    <option value="Pirot">Pirot </option>
                    <option value="Plandište">Plandište </option>
                    <option value="Platičevo">Platičevo </option>
                    <option value="Požarevac">Požarevac </option>
                    <option value="Požega">Požega </option>
                    <option value="Preševo">Preševo </option>
                    <option value="Priboj">Priboj </option>
                    <option value="Prijepolje">Prijepolje </option>
                    <option value="Prokuplje">Prokuplje </option>
                    <option value="Rakovica">Rakovica </option>
                    <option value="Rača">Rača </option>
                    <option value="Raška">Raška </option>
                    <option value="Rekovac">Rekovac </option>
                    <option value="Ruma">Ruma </option>
                    <option value="Rumenka">Rumenka </option>
                    <option value="Senta">Senta </option>
                    <option value="Sečanj">Sečanj </option>
                    <option value="Sivac">Sivac </option>
                    <option value="Sjenica">Sjenica </option>
                    <option value="Smedervska Palanka">Smedervska Palanka </option>
                    <option value="Smederevo">Smederevo </option>
                    <option value="Sokobanja">Sokobanja </option>
                    <option value="Sombor">Sombor </option>
                    <option value="Sopot">Sopot </option>
                    <option value="Srbobran">Srbobran </option>
                    <option value="Sremska Kamenica">Sremska Kamenica </option>
                    <option value="Sremska Mitrovica">Sremska Mitrovica </option>
                    <option value="Sremski Karlovci">Sremski Karlovci </option>
                    <option value="Stara Pazova">Stara Pazova </option>
                    <option value="Stari Banovci">Stari Banovci </option>
                    <option value="Starčevo">Starčevo </option>
                    <option value="Subotica">Subotica </option>
                    <option value="Surdulica">Surdulica </option>
                    <option value="Surčin">Surčin </option>
                    <option value="Svilajnac">Svilajnac </option>
                    <option value="Svrljig">Svrljig </option>
                    <option value="Temerin">Temerin </option>
                    <option value="Titel">Titel </option>
                    <option value="Topola">Topola </option>
                    <option value="Trstenik">Trstenik </option>
                    <option value="Tutin">Tutin </option>
                    <option value="Ub">Ub </option>
                    <option value="Užice">Užice </option>
                    <option value="Valjevo">Valjevo </option>
                    <option value="Varvarin">Varvarin </option>
                    <option value="Velika Plana">Velika Plana </option>
                    <option value="Veliko Gradište">Veliko Gradište </option>
                    <option value="Veternik">Veternik </option>
                    <option value="Vladimirci">Vladimirci </option>
                    <option value="Vladičin Han">Vladičin Han </option>
                    <option value="Vlasotince">Vlasotince </option>
                    <option value="Vranje">Vranje </option>
                    <option value="Vrbas">Vrbas </option>
                    <option value="Vrnjačka Banja">Vrnjačka Banja </option>
                    <option value="Vrčin">Vrčin </option>
                    <option value="Vršac">Vršac </option>
                    <option value="Zaječar">Zaječar </option>
                    <option value="Zemun">Zemun </option>
                    <option value="Zrenjanin">Zrenjanin </option>
                    <option value="Ćićevac">Ćićevac </option>
                    <option value="Ćuprija">Ćuprija </option>
                    <option value="Čajetina">Čajetina </option>
                    <option value="Čačak">Čačak </option>
                    <option value="Čenej">Čenej </option>
                    <option value="Čoka">Čoka </option>
                    <option value="Čukarica">Čukarica </option>
                    <option value="Čurug">Čurug </option>
                    <option value="Šabac">Šabac </option>
                    <option value="Šid">Šid </option>
                    <option value="Šimanovci">Šimanovci </option>
                    <option value="Žabalj">Žabalj </option>
                    <option value="Žabari">Žabari </option>
                    <option value="Žagubica">Žagubica </option>
                    <option value="Žitište">Žitište </option>
                    <option value="Žitorađa">Žitorađa </option>
                </select>
            </div>;

        return (
            <div className="form" id="form">
                <div className="container" id="registerUser">
                    <h2>{title}</h2> <br />
                    <form id="formUser">
                        {size}
                        <div className="form-group">
                            <label htmlFor="name">Naziv oglasa:</label>
                            <input type="text" className="form-control" placeholder="Unesite naziv oglasa" name="name" onChange={this.handleChange.bind(this, "name")} />
                        </div>
                        {city}
                        {message}
                        <div className="form-group">
                            <label htmlFor="image">Fotografija:</label> <br />
                            <input type="file" onChange={this.onImageChange} className="" id="group_image" />
                        </div>
                        <br />
                        <a href="#submit">
                            <button type="button" id="button" onClick={this.showObject} className="btn btn-success">Prikaži {title}</button>
                        </a>
                        <div>
                            {object}
                        </div>
                        <button id="submit" type="button" onClick={this.handleSubmit} className="btn btn-success">Sačuvajte</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default SaveObject;