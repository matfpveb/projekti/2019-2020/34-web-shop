import React, { Component } from 'react';
import '../css/Header.css'
import { withRouter } from "react-router-dom";
import axios from 'axios'
import Cookies from 'js-cookie';

class Header extends Component {

  constructor(init) {
    super(init);
    this.handleChange = this.handleChange.bind(this);
    this.searchPerName = this.searchPerName.bind(this);
    this.state = {
      scrolled: false,
      cities: [],
      otherCities: []
    };
    this.handleCity = this.handleCity.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      const isMinHeight = window.outerWidth > 750;
      if (isTop !== true && isMinHeight) {
        this.setState({ scrolled: true });
      } else if (isMinHeight) {
        this.setState({ scrolled: false });
      }
    });
    axios.get(`http://localhost:3001/mostCommonCity/`)
      .then(res => {
        let citiesArray = [];
        let otherCities = [];
        res.data.forEach(city => {
          citiesArray.push(city._id);
        });
        for (let i = 4; i < citiesArray.length; i++) {
          otherCities.push(<option key={i} value={citiesArray[i]}>{citiesArray[i]}</option>)
        }

        this.setState({
          cities: citiesArray,
          otherCities
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', () => {

    });

  }

  handleChange({ target }) {
    let nb = document.getElementById('nav_bar_response');
    if (target.checked) {
      nb.style.display = "block";
    } else {
      nb.style.display = "none";
    }
  }

  searchPerName(e) {
    if (e.key === 'Enter') {
      const name = e.target.value;
      if (name !== '') {
        const path = '/ads/' + name;
        this.props.history.push(path);
        this.props.history.go();
      }
    }
  }

  showInput() {
    let input = document.getElementById("name_input");
    input.style.display = "block";
    let city_btn = document.getElementById("city_buttons");
    if (window.innerWidth > 750) {
      city_btn.style.display = "flex";
    } else {
      city_btn.style.display = "block";
    }
  }

  exitInput() {
    let input = document.getElementById("name_input");
    input.style.display = "none";
    let city_btn = document.getElementById("city_buttons");
    city_btn.style.display = "none";
  }

  handleCity(city) {
    window.location.href = "/city/" + city.target.value;
  }
  logout() {
    Cookies.set('token', '');
  }

  render() {
    let buttons = [];
    let token = Cookies.get("token")
    if (token !== '') {
      buttons.push(<li key={1}><a key={1} href="/admin"> Dodaj oglas </a></li>);
      buttons.push(<li key={2}><a key={2} href="/"><button className="logout-btn" onClick={this.logout}> <span >Odjavite se</span></button> </a></li>);
    } else {
      buttons.push(<li key={3}><a key={3} href="/register"> Registrujte se </a></li>);
      buttons.push(<li key={4}><a key={4} href="/login" className="static_bar_response"> Prijavite se </a></li>);
    }
    return (
      <div className="header">
        <div className="btn_response">
          <label htmlFor="check" className="checkbtn">
            <input type="checkbox" id="check" onClick={this.handleChange}
              defaultChecked={this.props.complete} />
            <img src="/hamburger.png" className="img_bars" alt="fa_bars"></img>
          </label>
        </div>
        <button onClick={this.showInput} className="input_response">
          <img src="\search.png" alt="spotlight_photo" />
        </button>

        <div className={this.state.scrolled ? "first_box_scrolled" : "first_box"}>
          <div className="static_bar">
            <ul>
              {buttons}
            </ul>
          </div>
        </div>

        <div className="second_box">
          <div className={this.state.scrolled ? "nav_bar_scrolled" : "nav_bar"}>
            <a href="/" className="logo_header"> <img src="/logo.png" alt="logo_header" /> </a>
            <ul>
              <li> <a href="/#"> SVI OGLASI </a> </li>
              <li> <a href="/allProducts"> PROIZVODI </a> </li>
              <li> <a href="/allServices"> USLUGE </a> </li>
              <li> <a href="/contact"> KONTAKT </a> </li>
              <li><button onClick={this.showInput}> <img src="\search.png" alt="spotlight_photo" /> </button></li>
            </ul>
          </div>
        </div>

        <div className={this.state.scrolled ? "name_input_scrolled" : "name_input"}
          id="name_input">
          <input placeholder="Unesite sta zelite da pretrazujete" onKeyDown={this.searchPerName} />
          <button className="exit_input" onClick={this.exitInput}>
            <img src="\icon_close.png" alt="iconClose_photo" />
          </button>
        </div>

        <div className="nav_bar_response" id="nav_bar_response">
          <ul>
            <li> <a href="/"> SVI OGLASI </a> </li>
            <li> <a href="/#"> PROIZVODI </a> </li>
            <li> <a href="/#"> USLUGE </a> </li>
            <li> <a href="/#"> USLOVI KORIŠĆENJA </a> </li>
          </ul>
        </div>

        <div className="city_buttons" id="city_buttons">
          <a className="cities" href={`/city/${this.state.cities[0]}`}><button> {this.state.cities[0]} </button></a>
          <a className="cities" href={`/city/${this.state.cities[1]}`}><button> {this.state.cities[1]} </button></a>
          <a className="cities" href={`/city/${this.state.cities[2]}`}><button> {this.state.cities[2]} </button></a>
          <a className="cities" href={`/city/${this.state.cities[3]}`}><button> {this.state.cities[3]} </button></a>
          <span className="cities" >
            <button className="btn-other-cities">
              <select name="cities" className="other-cities" onChange={this.handleCity.bind(this)}>
                <option value="">Ostali</option>
                {this.state.otherCities}
              </select>
            </button>
          </span>
        </div>
      </div>
    );
  }
}

export default withRouter(Header);