import axios from 'axios';

export const register = newUser => {
    return axios
        .post('http://localhost:3001/register', {
            email: newUser.email,
            password: newUser.password
        })
        .then(res => {
            console.log(res);
        });
}

export const login = user => {
    return axios
        .post('http://localhost:3001/login', {
            email: user.email,
            password: user.password
        })
        .then(res => {
            localStorage.setItem('usertoken', res.data);
            return res.data;
        })
        .catch(err => {
            window.alert("Korisnik nije pronadjen!");
            console.log(err)
        });
}