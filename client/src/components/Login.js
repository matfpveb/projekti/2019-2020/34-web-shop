import React, { Component } from 'react';
import '../css/Login.css';
import axios from 'axios';
import Cookies from 'js-cookie';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                email: '',
                password: ''
            }
        };
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    checkValidityOfEmailAndPassword(entity) {
        const validationEmailRegex = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$");

        if (entity.email === '' || !validationEmailRegex.test(entity.email)) {
            window.alert("Uneti mail nije validan!");
            return false;
        } else if (entity.password === '') {
            window.alert("Uneti mail nije validan!");
            return false;
        }
        return true;
    }

    onEmailChange(email) {
        const user = {
            email: email.target.value,
            password: this.state.user.password
        }
        this.setState({
            user
        });
    }

    onPasswordChange(password) {
        const user = {
            email: this.state.user.email,
            password: password.target.value
        };
        this.setState({
            user
        });
    }

    handleSubmit() {
        const user = this.state.user;

        let valid = this.checkValidityOfEmailAndPassword(user);
        if (!valid) {
            return;
        }
        axios.post('http://localhost:3001/login', user)
            .then((response) => {
                Cookies.set('token', response.data.token, { expires: 1 });
                Cookies.set('email', this.state.user.email);
                document.getElementById("formUser").reset();
                window.location.href = "/admin";

            })
            .catch((error) => {
                window.alert("Pogrešna šifra");
                document.getElementById("formUser").reset();
                console.log(error);
            });
    }

    render() {
        return (
            <div className="login">
                <a href='/' className="back"><FontAwesomeIcon icon={faAngleLeft} className="fi_menu" /> Vratite se na početnu</a>
                <div className="container" id="logInUser">
                    <h2>Prijavite se</h2>
                    <form id="formUser">
                        <div className="form-group">
                            <label htmlFor="email">Email:</label>
                            <input type="email" className="form-control" id="emailUser" placeholder="Unesite email" name="email" onChange={this.onEmailChange} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="pwd">Šifra:</label>
                            <input type="password" className="form-control" id="pwdUser" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordChange} />
                        </div>
                        <button type="button" onClick={this.handleSubmit} className="btn btn-success">Prijavite se</button>
                    </form>
                </div>
            </div>
        );
    }
}