import React from 'react';
import './../App.css';
import './../css/Privacy.css';
import Header from './Header';
import Footer from './Footer';

class Privacy extends React.Component {

    render() {
        return (
            <div>
                <Header />
                <div className="privacyContainer">
                    <p>
                        <h3>Prava i obaveze profesionalnih prodavaca</h3> <br /><br />
                        Ukoliko ste profesionalni prodavac, a još uvek niste registrovani za tu delatnost, naša preporuka je da to učinite što pre! Ova stranica je osmišljena kao kratki vodič kroz temu profesionalnog bavljenja prodajom, na našem portalu - ali i van njega. Linkove ka dodatnim resursima koji se pominju u okviru teksta možete naći na dnu stranice.
                        <br /><br />
                        <h4>Ko je profesionalni prodavac?</h4> <br /><br />
                        Profesionalni prodavac ste ukoliko Vam prodaja dobara ili usluga predstavlja stalan izvor prihoda iz meseca u mesec. U tom slučaju ste i u zakonskoj obavezi da se registrujete i odaberete pravnu formu kojom ćete biti predstavljeni na tržištu i pred zakonom.
                        <br /><br />
                        Nasuprot tome - ako tu i tamo prodajete poneku ličnu stvar: loptu, patike, ili drugu polovnu robu u manjem obimu, onda niste profesionalni prodavac.
                        <br /><br />
                        <h4>Kako se registrovati?</h4> <br /><br />
                        Ako se prepoznajete u prvoj grupi, preporučujemo da se registrujete Agenciji za Privredne Registre, popunjavanjem jedinstvene registracione prijave. Možete se registrovati kao preduzetnik ili kao privredno društvo.
                        <br /><br />
                        Ukoliko odaberete preduzetničku formu, biće Vam potrebno poslovno ime, dokumentacija poput dokaza o identitetu preduzetnika, popunjena registraciona prijava i uplata odgovarajuće takse. Ako se odlučite za privredno društvo, trebaće Vam overen osnivački akt i uplaćen osnivački kapital. Više detalja o obe pravne forme i samom procesu registrovanja pronađite u sekciji Dodatni resursi i linkovi.
                        <br /><br />
                        Prilikom registracije ćete izabrati i šifru delatnosti. Najčešće će to biti “Trgovina na malo posredstvom pošte ili interneta”, ali niste u obavezi da baš to navedete kao pretežnu delatnost. Nezavisno od toga da li se registrujete kao preduzetnik ili kao privredno društvo, neophodno je da otvorite poseban račun kod banke i prijavite se nadležnoj filijali Poreske uprave.
                        <br /><br />
                        <h4>Prednosti statusa profesionalnog prodavca</h4> <br /><br />
                        Prednosti koje su na našoj platformi za oglašavanje rezervisane samo za preduzetnike i privredna društva su:
                        <br /><br />
                        Postavljanje oglasa sa stanjem NOVO<br />
                        Oglašavanje u svim grupama sekcije Usluge<br />
                        Oglašavanje u kategoriji Oprema u Zdravstvu<br />
                        Oglašavanje u grupi Veterinarska Oprema<br />
                        Oglašavanje u grupi Hrana za kućne ljubimce<br />
                        Profesionalni prodavci koji su registrovani kao pravna lica na našoj platformi postavljanjem svojih oglasa rade i na izgradnji svog sopstvenog brenda, budući da kupci mogu svaku prodaju da asociraju sa njihovim biznisom.
                        <br /><br />
                        Takođe, za malu privredu na našoj platformi postoji i posebno osmišljen program koji nudi jednostavnije rezervisanje reklamne kampanje, napravljeno po meri Vašeg biznisa.
                        <br /><br />
                        <h4>Obaveze profesionalnog prodavca</h4> <br /><br />
                        Što se tiče obaveza, one se odnose na uže poslovanje preduzetnika i privrednih društava i prvenstveno su izlistane u zakonima:
                        <br /><br />
                        Zakon o obligacionim odnosima<br />
                        Zakon o trgovini<br />
                        Zakon o elektronskoj trgovini<br />
                        Zakon o zaštiti potrošača<br />
                        Naša preporuka za sve profesionalne prodavce je da se dobro upoznaju sa ova četiri zakona, jer vam to može biti od velike koristi.
                        <br /><br />
                        Izdvajamo za Vas neke od osnovnih obaveza koje se tiču registrovanih preduzetnika i privrednih društava:
                        <br /><br />
                        Izdavanje računa - kako domaćim kupcima, tako i strancima. Račun izdajete bilo u fizičkom obliku, bilo elektronski, a pečat nije neophodan. Uz račun, kupcu ste dužni da izdate i otpremnicu sa podacima o kupcu, prodavcu, robi i potpisima kupca, prodavca i prevoznika.
                        <br /><br />
                        Takođe, uz račun ide i jednostrani raskid ugovora, na osnovu kog kupac može vratiti kupljenu stvar u roku od 14 dana od obavljene kupovine nezavisno od osnova. Ukoliko se radi o povraćaju po osnovu nesaobraznosti, prodavac je u obavezi da snosi troškove prevoza robe. Ako je u pitanju neki drugi osnov povraćaja, kupac snosi troškove transporta.
                        <br /><br />
                        Ukoliko Vaš godišnji promet prevazilazi 8 miliona dinara, onda ste u obavezi biti u sistemu PDV. Ta obaveza je univerzalna, nezavisno od toga da li se radi o privrednom društvu, ili o preduzetniku. Zanimljivost: Ukoliko se kupoprodaja roba ili usluga obavlja sa klijentom iz inostranstva, onda je ta kupoprodaja oslobođena PDV-a.
                        <br /><br />
                        Ukoliko sami isporučujete robu koju prodajete, morate koristiti fiskalnu kasu. Ukoliko je šaljete kurirskom službom, onda ne postoji takva obaveza.
                        <br /><br />
                        Morate voditi poslovne knjige, jer ne možete biti paušalno oporezovani. Takođe, morate voditi i posebnu knjigu prometa na KEP (knjiga evidencije prometa) obrascu, koja se vodi u elektronskoj formi. S tim u vezi dužni ste i prijaviti fizičku adresu nadležnim organima, kako bi ovi mogli obaviti uvid u pomenutu knjigu.
                        <br /><br />
                        <h4>Dodatni resursi i linkovi</h4> <br /><br />
                        Objašnjenje postupaka registracije preduzetnika i privrednog društva možete do detalja pročitati na sledeće dve stranice:<br />
                        Vodič za osnivanje preduzetnika- MojaFirma.rs<br />
                        Vodič za osnivanje d.o.o.- MojaFirma.rs<br />
                        Agencija za Privredne Registre:<br />
                        Jedinstvena registraciona prijava - APR<br />
                        Video materijali:<br />
                        E-commerce - prodaja preko interneta - Pojačalo vodič<br />
                        Porez na dodatu vrednost - Pojačalo vodič<br />
                    </p>
                </div>
                <Footer />
            </div>
        );
    }
}

export default Privacy;
