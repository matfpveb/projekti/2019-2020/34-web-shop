import React, { Component } from 'react';
import '../css/Register.css';
import axios from 'axios';
import Header from './Header';
import Footer from './Footer';
import ReturnToTop from './ReturnToTop';

export default class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                email: '',
                msg: ''
            }
        };

        this.onEmailChange = this.onEmailChange.bind(this);
        this.onMsgChange = this.onMsgChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onEmailChange(email) {
        const user = {
            email: email.target.value,
            msg: this.state.msg
        }
        this.setState({
            user
        });
    }

    onMsgChange(msg) {
        const user = {
            email: this.state.user.email,
            msg: msg.target.value
        }
        this.setState({
            user
        });
    }

    handleSubmit() {
        const user = this.state.user;
        if (user.email === '') {
            window.alert("Morate uneti email");
        } else if (user.msg === '') {
            window.alert("Morate uneti poruku");
        } else {
            axios.post('http://localhost:3001/contact', user)
                .then((_response) => {
                    window.alert("Mejl je poslat")
                    document.getElementById("formUser").reset();
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    render() {
        return (
            <div className="contactContainer">
                <Header />
                <div className="registry">
                    <div className="container" id="logInUser">
                        <h2>Kontaktirajte nas</h2>
                        <form id="formUser">
                            <div className="form-group">
                                <label htmlFor="email">Email:</label>
                                <input type="email" className="form-control" id="emailUser" placeholder="Unesite email" name="email" onChange={this.onEmailChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="formMessage">Poruka</label>
                                <textarea className="form-control" id="formMessage" rows="3" placeholder="Unesite poruku" name="formMessage" onChange={this.onMsgChange}></textarea>
                            </div>

                            <button type="button" onClick={this.handleSubmit} className="btn btn-success" id="contactBtn">Posaljite email</button>
                        </form>

                    </div>

                </div>
                <ReturnToTop />
                <Footer />
            </div>
        );
    }
}