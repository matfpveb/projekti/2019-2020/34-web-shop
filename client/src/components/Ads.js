import React from 'react';
import './../App.css';
import Header from './Header';
import Footer from './Footer';
import axios from 'axios';
import ServicesLSimplified from './ServicesL';
import ServicesMSimplified from './ServicesM';
import moment from 'moment';
import ProductMSimplified from './ProductM';
import ProductLSimplified from './ProductL';
import ReturnToTop from './ReturnToTop';

class Ads extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allAds: [],
    };
  }
  componentDidMount() {
    axios.get(`http://localhost:3001/product/getAllProducts/`)
      .then(res => {
        let allAds = [];
        let allProducts = [];
        let i = 0;

        res.data.forEach(d => {
          i = i + 1;
          allAds.push({
            type: 'product',
            name: d.name,
            image: d.image,
            message: d.message,
            dateOfCreateProduct: d.dateOfCreateProduct,
            size: d.sizeOfProduct
          })

        });
        axios.get(`http://localhost:3001/service/getAllServices`)
          .then(response => {
            response.data.forEach(d => {
              allAds.push({
                type: 'service',
                name: d.name,
                image: d.image,
                message: d.message,
                dateOfCreateProduct: d.dateOfCreateProduct,
                size: d.sizeOfservice
              })

            });
            allAds.sort((a, b) => {
              return new Date(b.dateOfCreateProduct) - new Date(a.dateOfCreateProduct);
            });
            let k = 0;
            allAds.forEach(d => {
              k = k + 1;
              if (d.type === 'product') {
                if (d.size === "m") {
                  allProducts.push(<ProductMSimplified key={k} name={d.name}
                    image={d.image}
                    message={d.message}
                    dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
                } else if (d.size === "l") {
                  allProducts.push(<ProductLSimplified key={k} name={d.name}
                    image={d.image}
                    message={d.message}
                    dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
                }
              } else {
                if (d.size === "m") {
                  allProducts.push(<ServicesLSimplified key={k} name={d.name}
                    image={d.image}
                    message={d.message}
                    dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
                } else if (d.size === "l") {
                  allProducts.push(<ServicesMSimplified key={k} name={d.name}
                    image={d.image}
                    message={d.message}
                    dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
                }
              }
            });
            this.setState({
              allAds: allProducts
            })
          })
      });
  }
  render() {
    return (
      <div>
        <Header />
        <div className="ProductsPerCity">
          {this.state.allAds}
        </div>
        <ReturnToTop />
        <Footer />
      </div>
    );
  }
}

export default Ads;