import React, { Component } from 'react';
import '../css/ServicesM.css'

class ServicesM extends Component {

  render() {
    return (
      <div className='ServicesM'>
        <div className='ServicesMContent'>
          <div className="ServicesMBox">
            <img src={this.props.image} alt="ProductPhoto" />
            <h3> {this.props.name} </h3>
            <p> {this.props.message} </p>
          </div>
          <p id="dateOfPublishing"> {this.props.dateOfCreateProduct} </p>
        </div>
      </div>
    );
  }
}

export default ServicesM;