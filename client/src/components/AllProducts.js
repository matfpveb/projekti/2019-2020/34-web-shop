import React from 'react';
import './../App.css';
import './../css/ProductPerCity.css';
import Header from './Header';
import Footer from './Footer';
import axios from 'axios';
import ProductMSimplified from './ProductM';
import ProductLSimplified from './ProductL';
import moment from 'moment';
import ReturnToTop from './ReturnToTop';

class AllProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }
  componentDidMount() {
    axios.get(`http://localhost:3001/product/getAllProducts/`)
      .then(res => {
        let products = [];
        let i = 0;

        res.data.forEach(d => {
          i = i + 1;
          if (d.sizeOfProduct === "m") {
            products.push(<ProductMSimplified key={i} name={d.name}
              image={d.image}
              message={d.message}
              dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
          } else if (d.sizeOfProduct === "l") {
            products.push(<ProductLSimplified key={i} name={d.name}
              image={d.image}
              message={d.message}
              dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
          }
        });
        this.setState({ products })
      });
  }

  render() {

    return (
      <div>
        <Header />
        <div className="ProductsPerCity">
          {this.state.products}
        </div>
        <ReturnToTop />
        <Footer />
      </div>
    );
  }
}

export default AllProducts;