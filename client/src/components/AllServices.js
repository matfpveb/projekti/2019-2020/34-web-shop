import React from 'react';
import './../App.css';
import Header from './Header';
import Footer from './Footer';
import axios from 'axios';
import ServicesLSimplified from './ServicesL';
import ServicesMSimplified from './ServicesM';
import moment from 'moment';
import ReturnToTop from './ReturnToTop';

class AllServices extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      services: []
    };
  }
  componentDidMount() {
    axios.get(`http://localhost:3001/service/getAllServices`)
      .then(res => {
        let services = [];
        let i = 0;
        res.data.forEach(d => {
          i = i + 1;
          if (d.sizeOfservice === "m") {
            services.push(<ServicesLSimplified key={i} name={d.name}
              image={d.image}
              message={d.message}
              dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
          } else if (d.sizeOfservice === "l") {
            services.push(<ServicesMSimplified key={i} name={d.name}
              image={d.image}
              message={d.message}
              dateOfCreateProduct={moment(d.dateOfCreateProduct).format('DD.MM.YYYY')} />);
          }
        });
        this.setState({ services })
      });
  }
  render() {
    return (
      <div>
        <Header />
        <div className="ProductsPerCity">
          {this.state.services}
        </div>
        <ReturnToTop />
        <Footer />
      </div>
    );
  }
}

export default AllServices;