import React, { Component } from 'react';
import '../css/ProductL.css'

class ProductL extends Component {

  render() {
    return (
      <div className='ProductL' href="city">
        <div className='ProductLContent'>
          <div className="ProductLBox">
            <img src={this.props.image} alt="ProductPhoto" />
            <h3> {this.props.name} </h3>
            <p> {this.props.price} </p>
            <p> {this.props.message} </p>
          </div>
          <p id="dateOfPublishing"> {this.props.dateOfCreateProduct} </p>
        </div>
      </div>
    );
  }
}

export default ProductL;