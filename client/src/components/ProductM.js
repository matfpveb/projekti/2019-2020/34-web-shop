import React, { Component } from 'react';
import '../css/ProductM.css'

class ProductM extends Component {

  render() {
    return (
      <div className='ProductM'>
        <div className='ProductMContent'>
          <div className="ProductMBox">
            <img src={this.props.image} alt="ProductPhoto" />
            <h3> {this.props.name} </h3>
            <p> {this.props.price} </p>
            <p> {this.props.message} </p>
          </div>
          <p id="dateOfPublishing"> {this.props.dateOfCreateProduct} </p>
        </div>
      </div>
    );
  }
}

export default ProductM;